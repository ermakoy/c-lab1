//
// Created by ermakoy on 21.09.17.
//

#ifndef LAB1FIX_INIPARSER_H
#define LAB1FIX_INIPARSER_H

#include <fstream>
#include <iostream>
#include <map>
#include "exeptions.h"

using namespace std;

class InIParser {
public:
    InIParser(string filename) throw(exc_io);

    ~InIParser();

    ifstream fin;
    map<string, map<string, string>> storage;
    string section;

    string DeleteCommentary(string) const;

    void ParseIt(string);

    void LetTheCarnageBegins();

    void TestPrint();

    string get_str_value(string, string) throw(exc_param);

    int get_int_value(string, string) throw(exc_param);

    float get_float_value(string, string) throw(exc_param);

    bool section_name_exist(string) throw(exc_param);

    bool param_exist(string, string) throw(exc_param);
};

#endif //LAB1FIX_INIPARSER_H
