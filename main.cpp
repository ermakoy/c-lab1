#include <iostream>
#include "InIParser.h"

int main() {
    try{
        InIParser parser("lab01_input_correct_09.ini");
        parser.LetTheCarnageBegins();
        parser.TestPrint();
        cout<<parser.get_float_value("DEMODS","BufferLenSeconds")<<endl;
        cout<<parser.get_int_value("COMMON","ListenTcpPort")<<endl;
        cout<<parser.get_str_value("DEMODS","FileName")<<endl;
        cout<<parser.get_float_value("1","1")<<endl;
    }catch(exc_io& e){
        cout<<e.what();
    }catch(exc_param& e){
        cout<<e.what();
    }
    return 0;
}